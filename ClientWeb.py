de importación de matraz , render_template, solicitud

importar redis
PalabraClave = "palabra"
DefinicionClave = "definicion"

r = redis. Redis(host='127.0.0.1', puerto=6379)
r. conjunto("id", -1)


def VerificarPalabraExistente(palabra):
    CantPalabras = r. llen(PalabraClave)
    PalabraExistente = Falso
    para i en rango(CantPalabras):
        PalabraActual = r. lindex(PalabraClave, i). decodificar('utf-8')
        if(PalabraActual == palabra):
            PalabraExistente = Verdadero
            quebrar
    volver PalabraExistente


def AgregarPalabraDef(palabra, definicion):
    r. incr("id")
    r. rpush(PalabraClave, palabra)
    r. rpush(DefinicionClave, definicion)
    print("\n palabra agregada correctamente!")


def ActualizarPalabra(AntiguaPalabra, NuevaPalabra, NuevaDefinicion):
    CantPalabras = r. llen(PalabraClave)
    para i en rango(CantPalabras):
        PalabraActual = r. lindex(PalabraClave, i). decodificar('utf-8')
        if(PalabraActual == AntiguaPalabra):
            r. lset(PalabraClave, i, NuevaPalabra)
            r. lset(DefinicionClave, i, NuevaDefinicion)
            quebrar

    imprimir("\n ! La palabra" + AntiguaPalabra + "fue actualizada!")


def EliminarPalabra(palabra):
    CantPalabras = r. llen(PalabraClave)
    para i en rango(CantPalabras):
        PalabraActual = r. lindex(PalabraClave, i). decodificar('utf-8')
        DefinicionActual = r. lindex(DefinicionClave, i). decodificar('utf-8')
        if(PalabraActual == palabra):
            r. lrem(PalabraClave, i, PalabraActual)
            r. lrem(DefinicionClave, i, DefinicionActual)
            quebrar
    print("\n ¡Palabra eliminada!")


def ShowAllWords():
    CantPalabras = r. llen(PalabraClave)
    palabras = []

    para i en rango(CantPalabras):
        palabras. anexar({"nombre": r. lindex(PalabraClave, i). descodificar(
            "utf-8"), "definicion": r. lindex(DefinicionClave, i). decodificar("utf-8")})
    volver palabras


print(r.keys())

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("Index.html")


@app.route('/AgregarPalabra', methods=['GET', 'POST'])
def AgregarPalabra():
    if request.method == 'POST':
        palabra = request.form["word"]
        definicion = request.form["meaning"]
        if VerificarPalabraExistente(palabra) == False:
            AgregarPalabraDef(palabra, definicion)
            return render_template("AgregarPalabra.html", message="!!Palabra añadida :)")
        else:
            return render_template("AgregarPalabra.html", message="!!La palabra ya existe :(")

    return render_template("AgregarPalabra.html")


@app.route('/EditarPalabra', methods=['GET', 'POST'])
def EditarPalabra():
    if request.method == 'POST':
        AntiguaPalabra = request.form["oldWord"]
        NuevaPalabra = request.form["word"]
        NuevaDefinicion = request.form["meaning"]

        if VerificarPalabraExistente(AntiguaPalabra):
            ActualizarPalabra(AntiguaPalabra, NuevaPalabra, NuevaDefinicion)

            return render_template("EditarPalabra.html", message=False)
        else:

            return render_template("EditarPalabra.html", message=True)

    return render_template("EditarPalabra.html")


@app.route('/EliminarPalabra', methods=['GET', 'POST'])
def eliminarPalabra():
    if request.method == 'POST':
        palabra = request.form["word"]

        if VerificarPalabraExistente(palabra):
            EliminarPalabra(palabra)
            ShowAllWords()
            return render_template("EliminarPalabra.html", message=False)
        else:
            ShowAllWords()
            return render_template("EliminarPalabra.html", message=True)

    return render_template("EliminarPalabra.html")


@app.route('/ListadoPalabras', methods=['GET', 'POST'])
def listadoPalabra():
    allPalabras = ShowAllWords()

    return render_template("ListadoPalabras.html", palabras=allPalabras)


@app. ruta('/BuscarSignificado', métodos=['GET', 'POST'])
def BuscarSignificado():
    si se solicita. método == 'POST':
        palabra = solicitud. forma["palabra"]
        si VerificarPalabraExistente(palabra):
            CantPalabras = r. llen(PalabraClave)
            para i en rango(CantPalabras):
                PalabraActual = r. lindex(PalabraClave, i). decodificar('utf-8')
                if(PalabraActual == palabra):
                    getPalabra = {"palabra": palabra, "definicion": r. lindex(
                        DefinicionClave, i). decodificar("utf-8")}

                    return render_template("BuscarSignificado.html", ShowWord=getPalabra)
        else:
            return render_template("BuscarSignificado.html", message=True)
    render_template de retorno ("BuscarSignificado.html")


si __name__ == "__main__":
    aplicación. ejecutar(depurar=Verdadero)